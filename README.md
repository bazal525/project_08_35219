# Opis programu

Program umozliwia skladanie zamowien w pizzerii. 
W glownym menu uzytkownik ma trzy mozliwości wyboru:

-Wybor produktow

-Finalizacja rachunku

-Wyjscie

Po wyborze pierwszej opcji, klient ma mozliwosc zamowienia pizzy/najpojow. 
Po przejsciu do finalizacji rachunku, klient widzi cale swoje zamowienie wraz z calkowitym kosztem.
Klient może zlozyc zamowienie (zakonczyc program) lub zresetowac rachunek i przejsc do menu glownego.
Wybranie opcji wyjscie powoduje zakonczenie programu.

Do zrealizowania projektu wykorzystane zostalo srodowisko QT. Aplikacja zostala napisana w oparciu o jezyk C++.

## Funkcje wykorzystane w projekcie:


Wywolywanie menu glownego
```bash
func()
```
Drukowanie rachunku
```bash
bill()
```
Promocja pizza 50/50
```bash
promo1()
```
Promocja "free drink"
```bash
promo3()
``` 
wybór między pizzą a napojem
```bash
menu()
``` 
Wybór rodzaju pizzy
```bash
pizza()
```
Wybór rodzaju napoju
```bash
drink()
```





## Angelika Król:
-Dodawanie opcji wyboru

-Dodanie logo

-Stworzenie plików txt

-Dodawanie pizz/napojów do rachunku

-Promocja nr3 + zabezpieczenie menu

-Naprawa błędów i optymalizacja końcowego kodu

-Tworzenie dokumentacji

## Damian Maziarz:
-Utworzenie projektu

-Wizualny pasek ładowania programu

-Dodawanie menu głównego

-Naliczanie ilości pizz i napojów

-Dodanie promocji nr1

-Dodanie promocji nr2

-Tworzenie dokumentacji