#include <iostream>
#include <time.h>
#include <conio.h>
#include <process.h>
#include <cstdlib>
#include <stdlib.h>
#include <cstring>
#include <windows.h>
#include<iomanip>
#include<fstream>

using namespace std;

float pizza1=0,pizza2=0,pizza3=0,pizza4=0,pizza5=0,pizza6=0;
unsigned int drink1=0,drink2=0,drink3=0,drink4=0,drink5=0;
unsigned int drink1_p=0,drink2_p=0,drink3_p=0,drink4_p=0,drink5_p=0;
float price1=17.00, price2=18.00, price3=22.00, price4=18.00, price5=14.00,price6=0;
float price_d1=5.00, price_d2=5.00, price_d3=5.00, price_d4=5.00, price_d5=4.00;
float flavour1=8.8, flavour2=9.00, flavour3=11.00, flavour4=9.00, flavour5=7.00;
char choice1, choice2;
int counter_pizza=0, counter_drink =0, free_drink=0;
float tot, discount;

void func();
void bill();
void menu();
void pizza();
void drinks();
void promo1();
void promo3();

int main()
{
    func();
}

void func()
{
    char loader=254;
    cout <<"\t\t\t\tLadowanie"<<endl;
    for(int i=0;i<10;i++)
    {
        system("color 0F");
        cout<<loader;
        Sleep(20);
    }
    system("cls");

    ifstream read1("logo.txt");
    string row;
    while (!read1.eof())
    {
        getline(read1,row);
        cout << row << endl;
    }
    read1.close();

    ifstream read4("promocje.txt");
    string row4;
    while(!read4.eof())
    {
        getline(read4, row4);
        cout << row4 << endl;
    }
    read4.close();

    cout << "\t\t\tWYBIERZ OD 1 DO 3 INACZEJ MENU SIE ZRESETUJE!!" <<endl;

    choice1=getche();

    if(choice1=='2')
    {
        system("cls");
        bill();
    }
    else if(choice1=='1')
    {
        system("cls");
        menu();
    }
    else if(choice1=='3')
    {
        system("cls");
        exit(0);
    }
    else
    {
        system("cls");
        func();
    }

}
void menu()
{
    ifstream read1("logo.txt");
    string row;
    while (!read1.eof())
    {
        getline(read1,row);
        cout << row << endl;
    }
    read1.close();

    cout<<"\n\t\t1. PIZZE\n\t\t2. NAPOJE\n";
    cout<<"\n\n\n\n\n\t\t\t\tKLIKNI R ABY WROCIC";

    do
    {
        choice2=getche();

        if(choice2=='1')
        {
            system("cls");
            pizza();
        }
        else
        {
            cout<<"Wybierz od 1 lub 2!"<<endl;
        }
        if(choice2=='2')
        {
            system("cls");
            drinks();
        }
        else
        {
            cout<<"Wybierz od 1 lub 2!"<<endl;
        }
    }
    while(choice2!='r' && choice2!=82);
    if(choice2=='r' || choice2==82)
    {
        system("cls");
        func();
    }
}

void pizza()
{
    char to_type;

    ifstream read1("logo.txt");
    string row;
    while (!read1.eof())
    {
        getline(read1, row);
        cout << row << endl;
    }
    read1.close();

    ifstream read2("menu.txt");
    string row2;
    while (!read2.eof())
    {
        getline(read2, row2);
        cout << row2 << endl;
    }
    read2.close();

    do
    {
        to_type=getche();

        switch(to_type)
        {
        case '1':
            cout<<" Pizza Capricciosa dodana do rachunku\n";
            pizza1++;
            break;
        case '2':
            cout<<" Pizza Pepperoni dodana do rachunku\n";
            pizza2++;
            break;
        case '3':
            cout<<" Pizza Hawajska dodana do rachunku\n";
            pizza3++;
            break;
        case '4':
            cout<<" Pizza Szynka Oliwka dodana do rachunku\n";
            pizza4++;
            break;
        case '5':
            cout<<" Pizza Margherita dodana do rachunku\n";
            pizza5++;
            break;
        case '6':
            promo1();
            break;
        default:
            cout<<"\n BLEDNE DANE WYBIERZ OD 1 DO 6\n";
            break;
        }

    }
    while(to_type!='r');
    if(to_type=='r')
    {
        system("cls");
        menu();
    }
}
void promo1()
{
    pizza6++;

    system("cls");
    ifstream read5("menupromocja.txt");
    string row5;
    while (!read5.eof())
    {
        getline(read5, row5);
        cout << row5 << endl;
    }
    read5.close();
    char to_type;
    int counter1=0;

    do
    {
        to_type=getche();

        switch(to_type)
        {
        case '1':
            cout<<" Wybrano smak Capricciosa\n";
            price6+=flavour1;
            counter1++;
            break;
        case '2':
            cout<<" Wybrano smak Pepperoni\n";
            price6+=flavour2;
            counter1++;
            break;
        case '3':
            cout<<" Wybrano smak Hawajska\n";
            price6+=flavour3;
            counter1++;
            break;
        case '4':
            cout<<" Wybrano smak Szynka Oliwka\n";
            price6+=flavour4;
            counter1++;
            break;
        case '5':
            cout<<" Wybrano smak Margherita\n";
            price6+=flavour5;
            counter1++;
            break;
        default:
            cout<<"\n BLEDNE DANE WYBIERZ OD 1 DO 5\n";
            break;
        }
    }
    while(counter1<=1 && to_type!='r');
    if(to_type=='r')
    {
        system("cls"),
                menu();
    }
    else
    {
        cout<<" PIZZA ZOSTALA WYBRANA, KLIKNIJ R ABY WROCIC";
    }
}

void drinks()
{
    ifstream read1("logo.txt");
    string row;
    while (!read1.eof())
    {
        getline(read1, row);
        cout << row << endl;
    }
    read1.close();

    ifstream read3("napoje.txt");
    string row3;
    while (!read3.eof())
    {
        getline(read3, row3);
        cout << row3 << endl;
    }
    read3.close();

    char to_type_d;

    do
    {
        to_type_d=getche();

        switch(to_type_d)
        {
        case '1':
            cout<<" Coca-Cola dodana do rachunku\n";
            drink1++;
            break;
        case '2':
            cout<<" Fanta dodana do rachunku\n";
            drink2++;
            break;
        case '3':
            cout<<" Coca-Cola ZERO dodana do rachunku\n";
            drink3++;
            break;
        case '4':
            cout<<" Sprite dodany do rachunku\n";
            drink4++;
            break;
        case '5':
            cout<<" Lemoniada Firmowa dodana do rachunku";
            drink5++;
            break;
        default:
            cout<<"\n BLEDNE DANE WYBIERZ OD 1 DO 5\n";
        }

    }
    while(to_type_d!='r');
    if(to_type_d=='r')
    {
        system("cls");
        menu();
    }
}
void promo3()
{
    system("cls");
    cout<<"\t\t\tWYBIERZ DARMOWY NAPOJ"<<endl;
    ifstream read3("napoje.txt");
    string row3;
    while (!read3.eof())
    {
        getline(read3, row3);
        cout << row3 << endl;
    }
    read3.close();

    char n=getche();

    switch (n)
    {
    case  '1':
        cout<<" wybrane\n";
        drink1++;
        drink1_p++;
        break;
    case '2':
        cout<<" wybrane\n";
        drink2++;
        drink2_p++;
        break;
    case  '3':
        cout<<" wybrane\n";
        drink3++;
        drink3_p++;
        break;
    case '4':
        cout<<" wybrane\n";
        drink4++;
        drink4_p++;
        break;
    case  '5':
        cout<<" wybrane\n";
        drink5++;
        drink5_p++;
        break;
    default:
        cout<<"\n BLEDNE DANE WYBIERZ OD 1 DO 5\n";
    }
}
void bill()
{
    int l=0;

    counter_pizza=pizza1+pizza2+pizza3+pizza4+pizza5+pizza6;
    free_drink=counter_pizza/2;
    counter_pizza=0;
    while(free_drink>0)
    {
        promo3();
        free_drink--;
    }
    system("cls");
    ifstream read1("logo.txt");
    string row;
    while (!read1.eof())
    {
        getline(read1, row);
        cout << row << endl;
    }
    read1.close();

    float total=0;
    time_t rawtime;struct tm * timeinfo;time ( &rawtime );timeinfo = localtime ( &rawtime );
    cout<< (asctime (timeinfo));
    cout<<"\nPIZZE/NAPOJE:"<<endl;
    cout<<"----------------------------------------------------------------------------------------"<<endl;


    //       ZAMOWIENIE PIZZ
    if(pizza1>0)
    {
        cout<<"Pizza Capricciosa   = "<<price1*pizza1<<"zl\t"<<"ILOSC PIZZ  = "<<pizza1<<"\n";
        total+=(price1*pizza1);
    }
    if(pizza2>0)
    {
        cout<<"Pizza Pepperoni     = "<<price2*pizza2<<"zl\t"<<"ILOSC PIZZ  = "<<pizza2<<"\n";
        total+=(price2*pizza2);
    }
    if(pizza3>0)
    {
        cout<<"Pizza Hawajska      = "<<price3*pizza3<<"zl\t"<<"ILOSC PIZZ  = "<<pizza3<<"\n";
        total+=(price3*pizza3);
    }
    if(pizza4>0)
    {
        cout<<"Pizza Szynka Oliwka = "<<price4*pizza4<<"zl\t"<<"ILOSC PIZZ  = "<<pizza4<<"\n";
        total+=(price4*pizza4);
    }
    if(pizza5>0)
    {
        cout<<"Pizza Margherita    = "<<price5*pizza5<<"zl\t"<<"ILOSC PIZZ  = "<<pizza5<<"\n";
        total+=(price5*pizza5);
    }
    if(pizza6>0)
    {
        cout<<"Pizza 50/50         = "<<price6*pizza6<<"zl\t"<<"ILOSC PIZZ  = "<<pizza6<<"\n";
        total+=(price6*pizza6);
    }

    //    ZAMÓWIENIE NAPOJI

    if(drink1>0)
    {
        cout<<"Coca-Cola           = "<<price_d1*(drink1-drink1_p)<<"zl\t"<<"ILOSC NAPOJU = "<<drink1<<"\n";
        total+=(price_d1*(drink1-drink1_p));

    }
    if(drink2>0)
    {
        cout<<"Fanta               = "<<price_d2*(drink2-drink2_p)<<"zl\t"<<"ILOSC NAPOJU = "<<drink2<<"\n";
        total+=(price_d2*(drink2-drink2_p));
    }
    if(drink3>0)
    {
        cout<<"Coca-Cola ZERO      = "<<price_d3*(drink3-drink3_p)<<"zl\t"<<"ILOSC NAPOJU = "<<drink3<<"\n";
        total+=(price_d3*(drink3-drink3_p));
    }
    if(drink4>0)
    {
        cout<<"Sprite              = "<<price_d4*(drink4-drink4_p)<<"zl\t"<<"ILOSC NAPOJU = "<<drink4<<"\n";
        total+=(price_d4*(drink4-drink4_p));
    }
    if(drink5>0)
    {
        cout<<"Lemoniada Firmowa   = "<<price_d5*(drink5-drink5_p)<<"zl\t"<<"ILOSC NAPOJU = "<<drink5<<"\n";
        total+=(price_d5*(drink5-drink1_p));
    }
    cout<<"\n----------------------------------------------------------------------------------------"<<endl;

    if(total > 100)
    {
        discount=total*0.80;
        cout<<"RACHUNEK CALKOWITY\t\t= "<<discount<<endl;

    }
    else
    {
        cout<<"RACHUNEK CALKOWITY\t\t= "<<total<<endl;
        tot+=total;
    }

    cout <<"\n\n\t\t\t\t\t\tKLIKNIJ E ABY ZAMOWIC LUB R ABY ZRESETOWAC RACHUNEK"<<endl;

    do{
        l=getche();
        switch(l)
        {
        case  'r':
            pizza1=0;
            pizza2=0;
            pizza3=0;
            pizza4=0;
            pizza5=0;
            pizza6=0;
            drink1=0;
            drink2=0;
            drink3=0;
            drink4=0;
            drink5=0;
            counter_drink=0;
            counter_pizza=0;
            tot = 0;
            system("cls");
            func();
            break;
        case 'e':
            system("cls");
            cout << endl;
            cout << endl;
            cout << endl;
            cout << " \t\t\t\t\t   ZAMOWIENIE ZOSTALO ZLOZONE" << endl;
            exit(3);
            break;
        default:
            cout<<"\n MOZESZ TYLKO ZRESETOWAC\n";

        }
    }
    while(l!='r' || l!='e');

}
